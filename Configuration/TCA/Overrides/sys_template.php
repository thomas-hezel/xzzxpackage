<?php
/**
 * Created by zazu.berlin
 * Author: thomas hezel
 * Date: 2019-05-09 / 20220426
 * This loads automatically static templates in root template
 * all Typoscript that is in the TypoScript folder
 * But you still must load it in Web > templates! According to latest 20220707 information.
 * Sometimes it works automatically
 */

defined('TYPO3') || die();

call_user_func(function()
    {
        /**
         * Extension key
         */
        $extensionKey = 'xzzxpackage';

        /**
         * Add default TypoScript (constants and setup)
         */
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
            $extensionKey,
            'Configuration/TypoScript',
            'xzzxpackage'
        );
    });
