<?php
declare(strict_types=1);

defined('TYPO3') or die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['zazu'] = 'EXT:zazupackage/Configuration/RTE/Default-RTE-package.yaml';

/***************
 * Add default RTE configuration
 *
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['zazu'] = 'EXT:xzzxpackage/Configuration/RTE/zazu-settings-RTE.yaml';
